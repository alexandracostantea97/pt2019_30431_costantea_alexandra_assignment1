package Model;

import static org.junit.jupiter.api.Assertions.*;

class PolynomialTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {

    }

    @org.junit.jupiter.api.Test
    void add() {

        Polynomial p1 = new Polynomial("2 0 3 2");
        Polynomial p2 = new Polynomial("1 3 2 1");
        p1.add(p2);
        assertEquals("3x^3 + 3x^2 + 5x^1 + 3x^0 ", p1.toString());

    }

    @org.junit.jupiter.api.Test
    void sub() {
        Polynomial p1 = new Polynomial("2 0 3 2");
        Polynomial p2 = new Polynomial("1 3 2 1");
        p1.sub(p2);
        assertEquals("1x^3 + -3x^2 + 1x^1 + 1x^0 ", p1.toString());
    }

    @org.junit.jupiter.api.Test
    void differentiate() {
        Polynomial p1 = new Polynomial("2 0 3 2");
        p1.differentiate();
        assertEquals("6x^2 + 3x^0 ", p1.toString());
    }

    @org.junit.jupiter.api.Test
    void integrate() {
        Polynomial p1 = new Polynomial("2 0 3 2");
        p1.integrate();
        assertEquals("1x^2 + 2x^1 ", p1.toString());
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        Polynomial p1 = new Polynomial("2 0");
        Polynomial p2 = new Polynomial("1 3");
        p1.multiply(p2);
        assertEquals("2x^2 + 6x^1 ", p1.toString());
    }

    @org.junit.jupiter.api.Test
    void divide() {
        Polynomial p1 = new Polynomial("2 0 3 2");
        Polynomial p2 = new Polynomial("1 3 2 1");
        p1.divide(p2);
        assertEquals("2x^0 ",p1.toString());
        assertEquals("-6x^2 + -1x^1 ",p2.toString());
    }
}