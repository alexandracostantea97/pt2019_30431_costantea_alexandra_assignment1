package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Frame extends JFrame {
	private JPanel labelPanel = new JPanel();
	private JPanel panel1 = new JPanel();
	private JPanel operations = new JPanel();
	private JPanel panel2 = new JPanel();
	private JTextField polynom1Tf = new JTextField("");
	private JTextField polynom2Tf = new JTextField("");
	private JButton integrateButton1 = new JButton("Integrate");
	private JButton diffButton1 = new JButton("Differentiate");
	private JButton integrateButton2 = new JButton("Integrate");
	private JButton diffButton2 = new JButton("Differentiate");
	private JButton addButton = new JButton("Add");
	private JButton subtractButton = new JButton("Subtract");
	private JButton mulButton = new JButton("Multiply");
	private JButton divButton = new JButton("Divide");
	
	
	public Frame(){
		super("Calculator");
		this.setLayout(new GridLayout(4,1));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(500, 600);
		panel1.add(polynom1Tf);
		panel1.add(diffButton1);
		panel1.add(integrateButton1);
		panel2.add(polynom2Tf);
		panel2.add(diffButton2);
		panel2.add(integrateButton2);
		operations.add(mulButton);
		operations.add(divButton);
		operations.add(addButton);
		operations.add(subtractButton);
		polynom1Tf.setColumns(30);
		polynom2Tf.setColumns(30);
		this.add(labelPanel);
		this.add(panel1);
		this.add(panel2);
		this.add(operations);
		this.pack();
		
	}

	public void addTf1Listener(ActionListener e){
		polynom1Tf.addActionListener(e);
	}

	public void addTf2Listener(ActionListener e){
		polynom2Tf.addActionListener(e);
	}

	public void addIntegrateButton1Listener(ActionListener e){
		integrateButton1.addActionListener(e);
	}

	public void addDifferentiateButton1Listener(ActionListener e){
		diffButton1.addActionListener(e);
	}

	public void addIntegrateButton2Listener(ActionListener e){
		integrateButton2.addActionListener(e);
	}

	public void addDifferentiateButton2Listener(ActionListener e){
		diffButton2.addActionListener(e);
	}

	public void addSubButtonListener(ActionListener e){
		subtractButton.addActionListener(e);
	}
	
	public void addAdditionButtonListener(ActionListener e){
		addButton.addActionListener(e);
	}

	public void addMulButtonListener(ActionListener e){
		mulButton.addActionListener(e);
	}
	
	public void addDivButtonListener(ActionListener e){
		divButton.addActionListener(e);
	}
	
	public JTextField getTf1(){
		return polynom1Tf;
	}
	
	public JTextField getTf2(){
		return polynom2Tf;
	}
}
