package Model;

public class MonomInt extends Monomial {

    public  MonomInt(int coeff, int degree){
        super(coeff,degree);
    }
    public  MonomInt(){
        super();
    }

    public void add(Monomial m) {
        if(this.getDegree()== m.getDegree()){
            this.setCoefficient(this.getCoefficient()+m.getCoefficient());
        }
        else{
            System.out.println("Trying to add monomials with different degrees");
        }
    }

    public void sub(Monomial m) {
        if(this.getDegree()== m.getDegree()){
            this.setCoefficient(this.getCoefficient()-m.getCoefficient());
        }
        else{
            System.out.println("Trying to sub monomials with different degrees");
        }
    }

    public void integrate() {
        this.setDegree(this.getDegree()+1);
        this.setCoefficient(this.getCoefficient()/this.getDegree());
    }

    public void differentiate() {
        this.setCoefficient(this.getCoefficient()*this.getDegree());
        this.setDegree(this.getDegree()-1);
    }

    public void multiply(Monomial m) {
        this.setCoefficient(this.getCoefficient()*m.getCoefficient());
        this.setDegree(this.getDegree()+m.getDegree());
    }

    public void divide(Monomial m) {
        this.setCoefficient(this.getCoefficient()/m.getCoefficient());
        this.setDegree(this.getDegree()-m.getDegree());
    }

    public String toString(){
        String S = "";
        S = this.getCoefficient() + "x^" +  this.getDegree();
        return S;
    }
}
