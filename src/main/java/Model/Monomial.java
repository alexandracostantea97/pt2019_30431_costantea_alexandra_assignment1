package Model;

public abstract class Monomial {
    private int Coefficient, Degree;

   public  Monomial(int coeff, int degree){
       Coefficient = coeff;
       Degree = degree;
   }
    public  Monomial(){
        Coefficient = 0 ;
        Degree = 0;
    }


   public abstract void add(Monomial m) ;
   public abstract void sub(Monomial m) ;
   public abstract void integrate() ;
   public abstract void differentiate() ;
   public abstract void multiply(Monomial m);
   public abstract void divide(Monomial m);

    public int getCoefficient() {
        return Coefficient;
    }

    public void setCoefficient(int coefficient) {
        Coefficient = coefficient;
    }

    public int getDegree() {
        return Degree;
    }

    public void setDegree(int degree) {
        Degree = degree;
    }
}
