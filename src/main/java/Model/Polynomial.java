package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Polynomial {
    private ArrayList<Monomial> Monomials = new ArrayList<Monomial>();

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("insert polynom1:");
        String s = scanner.nextLine();
        Polynomial p1 = new Polynomial(s);
        System.out.println("insert polynom1:");
        s = scanner.nextLine();
        Polynomial p2 = new Polynomial(s);
        p1.divide(p2);
        System.out.println(" cat:" + p1 + " res t: " + p2);

    }

   public Polynomial(String S) {
       String[] coefficients = S.split(" ");
       int len = coefficients.length;
       for(String i : coefficients){
           Monomial m = new MonomInt(Integer.parseInt(i),len -1 );
           len = len - 1;
           Monomials.add(m);
       }
       Collections.reverse(Monomials);

   }

    private Polynomial(int n) {
        for(int i = 0 ; i<= n; i++) {
            Monomials.add(new MonomInt(0,i));
        }
    }

    public void clean() {
        while(Monomials.size() > 1 & Monomials.get(this.Monomials.size() - 1).getCoefficient() == 0) {
            Monomials.remove(Monomials.size()-1);
        }
    }

   public Monomial get(int i){
       if (i>=0 && i<Monomials.size()){
           return Monomials.get(i);
       }
       else{
           return null;
       }
   }

    public void add(Polynomial pol2)
    {
        for(Monomial a:Monomials)
        {
            if(a.getDegree()<pol2.getMonomials().size())
            {
                a.add(pol2.getMonomials().get(a.getDegree()));
            }

        }
        if (Monomials.size()<pol2.Monomials.size())
        {
            for (int i=Monomials.size();i<pol2.Monomials.size();i++)
            {
                Monomials.add(new MonomInt(pol2.getMonomials().get(i).getCoefficient(),pol2.getMonomials().get(i).getDegree()));

            }
        }
    }

    public void sub(Polynomial pol2){
        for(Monomial a:Monomials)
        {
            if(a.getDegree()<pol2.getMonomials().size())
            {
                a.sub(pol2.getMonomials().get(a.getDegree()));
            }

        }
        if (Monomials.size()<pol2.Monomials.size())
        {
            for (int i=Monomials.size();i<pol2.Monomials.size();i++)
            {
                Monomials.add(new MonomInt(pol2.getMonomials().get(i).getCoefficient(),pol2.getMonomials().get(i).getDegree()));

            }
        }
    }

    public void differentiate(){
        for(Monomial a:Monomials)
        {
            a.differentiate();
        }
        Monomials.remove(0);
    }

    public void integrate()
    {
        for(Monomial a:Monomials)
        {
            a.integrate();
        }
    }

    public void multiply(Polynomial pol2){
        int productSize = this.Monomials.size() + pol2.Monomials.size();
        Polynomial product = new Polynomial(productSize);
        for(Monomial m : Monomials) {
            for(Monomial n: pol2.getMonomials()) {
                Monomial monom = new MonomInt(m.getCoefficient(), m.getDegree());
                monom.multiply(n);
                product.get(monom.getDegree()).add(monom);
            }
        }
        this.Monomials = product.getMonomials();
    }

    public void divide(Polynomial pol2) {
        if(pol2.Monomials.size() > 1 | pol2.get(0).getCoefficient() != 0) {
            Polynomial cat = new Polynomial(0);
            int size;
            Polynomial rest = this;
            while((rest.Monomials.size() > 1 | rest.get(0).getCoefficient() != 0) & rest.Monomials.size()>=pol2.Monomials.size()) {
                size = rest.get(rest.Monomials.size()-1).getDegree() - pol2.get(pol2.Monomials.size()-1).getDegree();
                Polynomial temp = new Polynomial(size);
                Monomial m = rest.get(rest.Monomials.size() - 1);
                m.divide(pol2.get(pol2.Monomials.size()-1));
                temp.getMonomials().set(size,m);
                cat.add(temp);
                temp.multiply(pol2);
                rest.sub(temp);
                cat.clean();
                rest.clean();
            }
            pol2.Monomials = rest.Monomials;
            this.Monomials= cat.getMonomials();
        }
    }

    public ArrayList<Monomial> getMonomials() {
        return Monomials;
    }

    public void setMonomials(ArrayList<Monomial> monomials) {
        Monomials = monomials;
    }

    public String toString(){
       String S = "";
       Collections.reverse(Monomials);
       for(Monomial m : Monomials){
           if(m.getCoefficient() == 0){
               continue;
           }
           else{
               S+=m.toString()+" + ";
           }
       }
        if(S.length()>0)
        {
            S=S.substring(0,S.length()-2);
        }
        else
        {
            S="0";
        }

        Collections.reverse(Monomials);
        return S;
    }
}
