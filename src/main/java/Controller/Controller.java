package Controller;

import Model.Polynomial;
import View.Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

	private Frame window;
	private Polynomial p1;
	private Polynomial p2;
	
	public static void main(String[] args) {
		
		Frame window = new Frame();
		Controller controller = new Controller(window);
		window.setVisible(true);
	}
	
	public Controller(Frame window){
		this.window = window;
		window.addMulButtonListener(new MultiplyListener());
		window.addDivButtonListener(new DivisionListener());
		window.addDifferentiateButton1Listener(new Diff1Listener());
		window.addDifferentiateButton2Listener(new Diff2Listener());
		window.addAdditionButtonListener(new AdditionListener());
		window.addSubButtonListener(new SubtractionListener());
		window.addIntegrateButton1Listener(new Integrate1Listener());
		window.addIntegrateButton2Listener(new Integrate2Listener());
		window.addTf1Listener(new Tf1Listener());
		window.addTf2Listener(new Tf2Listener());
	}


	class AdditionListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.add(p2);
			window.getTf1().setText(p1.toString());
		}
	}

	class SubtractionListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.sub(p2);
			window.getTf1().setText(p1.toString());
		}
	}

	class Diff1Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.differentiate();
			window.getTf1().setText(p1.toString());
		}
	}
	
	class Diff2Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p2.differentiate();
			window.getTf2().setText(p2.toString());
		}
	}
	
	class Integrate1Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.integrate();
			window.getTf1().setText(p1.toString());
		}
	}
	
	class Integrate2Listener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p2.integrate();
			window.getTf2().setText(p2.toString());
		}
	}
	
	class MultiplyListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.multiply(p2);
			window.getTf1().setText(p1.toString());
		}
	}
	
	class DivisionListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			p1.divide(p2);
			window.getTf1().setText(p1.toString());
			window.getTf2().setText(p2.toString());
		}
	}

	class Tf1Listener implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			p1 = new Polynomial(window.getTf1().getText());
			window.getTf1().setText(p1.toString());
		}
	}

	class Tf2Listener implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			p2 = new Polynomial(window.getTf2().getText());
			window.getTf2().setText(p2.toString());
		}
	}
}
